package main

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"encoding/asn1"
	"fmt"
	"io"
	"math/big"

	"github.com/google/go-tpm/tpm2"
	"github.com/google/go-tpm/tpmutil"
)

var (
	tmpl_rsa = tpm2.Public{
		Type:    tpm2.AlgRSA,
		NameAlg: tpm2.AlgSHA256,
		Attributes: tpm2.FlagFixedTPM | // Key can't leave the TPM
			tpm2.FlagFixedParent | // Key can't change parent
			tpm2.FlagSensitiveDataOrigin | // Key created by the TPM
			tpm2.FlagUserWithAuth | // Uses password, can be empty
			tpm2.FlagNoDA |
			tpm2.FlagRestricted | // Key uses for challenge-response protocol
			tpm2.FlagDecrypt, // Key can be used for decryption
		RSAParameters: &tpm2.RSAParams{
			Symmetric: &tpm2.SymScheme{
				Alg:     tpm2.AlgAES,
				KeyBits: 128,
				Mode:    tpm2.AlgCFB},
			KeyBits:    2048,
			ModulusRaw: make([]byte, 256), // Actual data necoded in the template
		},
	}

	tmpl_ecc = tpm2.Public{ // Change name to something more informatie
		Type:    tpm2.AlgECC,
		NameAlg: tpm2.AlgSHA256,
		Attributes: tpm2.FlagFixedTPM | // Key can't leave the TPM
			tpm2.FlagFixedParent | // Key can't change parent
			tpm2.FlagSensitiveDataOrigin | // Key created by the TPM
			tpm2.FlagUserWithAuth | // Uses password, can be empty
			tpm2.FlagSign, // Key can be used to sign data
		ECCParameters: &tpm2.ECCParams{
			Sign:    &tpm2.SigScheme{Alg: tpm2.AlgECDSA, Hash: tpm2.AlgSHA256},
			CurveID: tpm2.CurveNISTP256,
			Point: tpm2.ECPoint{
				XRaw: make([]byte, 32),
				YRaw: make([]byte, 32),
			},
		},
	}

	// tpmPath = "/dev/tpmrm0"
)

type signer struct {
	tpm    io.ReadWriter
	handle tpmutil.Handle
	pub    crypto.PublicKey
}

type Asset struct {
	ObjectType        string `json:"objectType"` // ObjectType is used to distinguish different object types in the same chaincode namespace
	ID                string `json:"assetID"`
	OwnerOrg          string `json:"ownerOrg"`
	PublicDescription string `json:"publicDescription"`
}

func (s *signer) Public() crypto.PublicKey {
	return s.pub
}

func (s *signer) Sign(r io.Reader, digest []byte, opts crypto.SignerOpts) ([]byte, error) {
	password := ""
	sig, err := tpm2.Sign(s.tpm, s.handle, password, digest, nil, nil)
	if err != nil {
		return nil, fmt.Errorf("can't sign the data: %v", err)
	}
	if sig.RSA != nil {
		return sig.RSA.Signature, nil
	}
	if sig.ECC != nil {
		return asn1.Marshal(struct {
			R *big.Int
			S *big.Int
		}{sig.ECC.R, sig.ECC.S})
	}
	return nil, fmt.Errorf("unsupported signature type: %v", sig.Alg)
}

func newSigner(tpm io.ReadWriter, handle tpmutil.Handle) (*signer, error) {
	tpmPub, _, _, err := tpm2.ReadPublic(tpm, handle)
	if err != nil {
		return nil, fmt.Errorf("can't read public blob: %v", err)
	}
	pub, err := tpmPub.Key()
	if err != nil {
		return nil, fmt.Errorf("can't decode the public key: %v", err)
	}
	return &signer{tpm, handle, pub}, nil
}

func signMsg(priv *signer, msg []byte) (*ecdsa.PublicKey, [32]byte, []byte, error) {
	digest := sha256.Sum256(msg)

	sig, err := priv.Sign(rand.Reader, digest[:], crypto.SHA256)
	if err != nil {
		return nil, [32]byte{}, nil, fmt.Errorf("can't sign the message: %v", err)
	}

	pub, ok := priv.Public().(*ecdsa.PublicKey)
	if !ok {
		return nil, [32]byte{}, nil, fmt.Errorf("expected ecdsa.Publickey got: %T", priv.Public())
	}

	return pub, digest, sig, nil
}

func verify(digest [32]byte, pub *ecdsa.PublicKey, sig []byte) error {
	if !ecdsa.VerifyASN1(pub, digest[:], sig) {
		return fmt.Errorf("failed to verify signature")
	}
	return nil
}

func createSigner(rwc io.ReadWriter) (*signer, error) {
	/* Create Storage root key and Application key*/

	// Create Storage root key which stays inside the tpm
	parentPass, ownerPass := "", ""
	storageRootKey, _, err := tpm2.CreatePrimary(rwc, tpm2.HandleOwner, tpm2.PCRSelection{}, parentPass, ownerPass, tmpl_rsa)
	if err != nil {
		return nil, fmt.Errorf("can't create storage root key: %v", err)
	}

	// Create key pair using the storage root key
	privBlob, pubBlob, _, _, _, err := tpm2.CreateKey(rwc, storageRootKey, tpm2.PCRSelection{}, parentPass, ownerPass, tmpl_ecc)
	if err != nil {
		return nil, fmt.Errorf("can't create the applicaton key: %v", err)
	}

	// Load the key pair into an object in the TPM
	// The appKey is a handle to
	parentAuth := ""
	appKey, _, err := tpm2.Load(rwc, storageRootKey, parentAuth, pubBlob, privBlob)
	if err != nil {
		return nil, fmt.Errorf("can't load private and public blobs into the TPM: %v", err)
	}

	// Create a new signer to sign data using the application key
	priv, err := newSigner(rwc, appKey)
	if err != nil {
		fmt.Printf("can't create a new signer: %v", err)
	}

	return priv, nil
}

func assetToByteArray(asset Asset) []byte {
	// Returns struct values in the from of:
	// {structValue1 structValue2 ...} then converts
	// this string into a byte array
	return []byte(fmt.Sprintf("%v", asset))
}

func SignAsset(rwc io.ReadWriter, asset Asset) (*ecdsa.PublicKey, []byte, error) {
	// Create signer t
	signer, err := createSigner(rwc)
	if err != nil {
		return nil, nil, fmt.Errorf("something went wrong while creating the signer: %v", err)
	}

	pub, _, sig, err := signMsg(signer, assetToByteArray(asset))
	if err != nil {
		return nil, nil, fmt.Errorf("something went wrong while signing the asset: %v", err)
	}

	return pub, sig, nil
}

func VerifyAsset(pub *ecdsa.PublicKey, asset Asset, sig []byte) bool {
	// Hash asset byte array
	digest := sha256.Sum256(assetToByteArray(asset))

	// Verify the digital signature
	if verify(digest, pub, sig) != nil {
		return false
	} else {
		return true
	}
}

// func test() {
// 	rwc, err := tpm2.OpenTPM(tpmPath)
// 	if err != nil {
// 		fmt.Printf("can't open tpm: %v", err)
// 	}
// 	defer func() error { // Notify user if the TPM was not closed
// 		if err := rwc.Close(); err != nil {
// 			return fmt.Errorf("can't close TPM %q: %v", tpmPath, err)
// 		}
// 		return nil
// 	}()

// 	asset := Asset{
// 		ObjectType:        "asset",
// 		ID:                "asset1",
// 		OwnerOrg:          "Org1MSP",
// 		PublicDescription: "A new asset for Org1MSP",
// 	}

// 	pub, sig, err := SignAsset(rwc, asset)
// 	if err != nil {
// 		fmt.Printf("Signing the asset didn't work\n")
// 	}
// 	fmt.Printf("Signing complete!\n")

// 	// asset.OwnerOrg = ""
// 	if VerifyAsset(pub, asset, sig) {
// 		fmt.Printf("Asset has not been tampered\n")
// 	} else {
// 		fmt.Printf("Something's fissshyyy\n")
// 	}
// }
